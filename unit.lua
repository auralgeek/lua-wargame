local Unit = {}

local new = function(x, y, unitType, team, attack, defense, HPmax, HP, APmax, AP)
	local obj = {
		x = x or 0,
		y = y or 0,
		unitType = unitType or 0,
		team = team or 0,
		attack = attack or 0,
		defense = defense or 0,
		HPmax = HPmax or 1,
		HP = HP or 1,
		APmax = APmax or 1,
		AP = AP or APmax
	}

	return setmetatable(obj, Unit)
end

local move = function(self, x, y)
	local movementCost = math.abs(self.x - x) + math.abs(self.y - y)
	if movementCost <= self.AP then
		self.x = x
		self.y = y
		self.AP = self.AP - movementCost
		return true
	end

	return false
end

local attack = function(self, target)
	self.takeDamage(target.defense)
	target.takeDamage(self.attack)
end


local tostring = function(self)
	return string.format("X: %d, Y: %d, Type: %d, Team: %d, Attack: %d, Defense: %d, HPmax: %d, HP: %d, APmax: %d, AP: %d", self.x, self.y, self.unitType, self.team, self.attack, self.defense, self.HPmax, self.HP, self.APmax, self.AP)
end

Unit.move = move
Unit.__tostring = tostring
Unit.__index = Unit
Unit.__metatable = {}

local constructor = function(cls, ...)
	return new(...)
end

return setmetatable({}, {__call = constructor})
