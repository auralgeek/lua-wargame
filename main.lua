local noise2d = require "simplex2d"
local Unit = require "unit"

local map
local mapWidth, mapHeight

local mapX, mapY

local tilesDisplayWidth, tilesDisplayHeight
local zoomX, zoomY

local tilesetImage
local unitTilesetImage
local tilesetSize
local tileQuads = {}
local unitTiles = {}
local unitsList = {}
local tilesetSprite
local selectedX = 0
local selectedY = 0
local selectedUnit
local secX = 0
local secY = 0
local numPlayers = 2
local playerTurn = 0
local turn = 1

function love.load()
	love.window.setTitle('War')
	love.window.setMode(1280, 1024)
	shader = love.graphics.newShader("shader.fs")
	setupMap()
	setupMapView()

	table.insert(unitsList, createUnit(12, 15, 'archer', 1))
	table.insert(unitsList, createUnit(14, 17, 'infantry', 0))
	table.insert(unitsList, createUnit(5, 5, 'cavalry', 1))

	setupTileset()
	setupUnitTileset()
	love.graphics.setFont(love.graphics.newFont(12))
end

function setupMap()
	mapWidth = 120
	mapHeight = 80

	math.randomseed(os.time())
	
	map = {}
	
	for x=1,mapWidth do
		map[x] = {}
		for y=1,mapHeight do
			map[x][y] = math.floor(3 * noise2d((x + 1000 * math.random(1, 1))/50, (y + 1000 * math.random(1, 1))/50) + 3.5)
		end
	end
end

function createUnit(x, y, unitType, team)
	if unitType == 'archer' then
		new_unit = Unit(x, y, team, team, 2, 0, 2, 2, 2, 2)
	elseif unitType == 'infantry' then
		new_unit = Unit(x, y, 2 + team, team, 2, 2, 4, 4, 2, 2)
	elseif unitType == 'cavalry' then
		new_unit = Unit(x, y, 4 + team, team, 4, 1, 4, 4, 4, 4)
	end

	return new_unit
end

function setupMapView()
	mapX = 1
	mapY = 1

	tileSize = 32
	zoomX = 1.0
	zoomY = 1.0
	tilesDisplayWidth = math.floor(love.graphics.getWidth() / (zoomX * tileSize)) + 1
	tilesDisplayHeight = math.ceil(love.graphics.getHeight() / (zoomY * tileSize)) + 1
end

function setupUnitTileset()
	unitTilesetImage = love.graphics.newImage("units.png")
	unitTilesetImage:setFilter("nearest", "linear")

	for i=0,6 do
		unitTiles[i] = love.graphics.newQuad(i * tileSize, 0, tileSize, tileSize, unitTilesetImage:getWidth(), unitTilesetImage:getHeight())
	end

	unitTilesetBatch = love.graphics.newSpriteBatch(unitTilesetImage, tilesDisplayWidth * tilesDisplayHeight)
	updateUnitTilesetBatch()
end

function updateUnitTilesetBatch()
	unitTilesetBatch:clear()
	for _, value in pairs(unitsList) do
		xpos = (value.x - math.floor(mapX))*tileSize
		ypos = (value.y - math.floor(mapY))*tileSize
		unitTilesetBatch:add(unitTiles[value.unitType], xpos, ypos)
	end
	unitTilesetBatch:flush()
end

function setupTileset()
	tilesetImage = love.graphics.newImage("colormap.png")
	tilesetImage:setFilter("nearest", "linear")
	tileSize = 32

	for i=0,6 do
		tileQuads[i] = love.graphics.newQuad(i * tileSize, 0, tileSize, tileSize, tilesetImage:getWidth(), tilesetImage:getHeight())
	end

	tilesetBatch = love.graphics.newSpriteBatch(tilesetImage, tilesDisplayWidth * tilesDisplayHeight)

	updateTilesetBatch()
end

function updateTilesetBatch()
	tilesetBatch:clear()
	for x=0,tilesDisplayWidth-1 do
		for y=0,tilesDisplayHeight-1 do
			tilesetBatch:add(tileQuads[map[x+math.floor(mapX)][y+math.floor(mapY)]], x*tileSize, y*tileSize)
		end
	end
	tilesetBatch:flush()
end

function moveMap(dx, dy)
	oldMapX = mapX
	oldMapY = mapY
	mapX = math.max(math.min(mapX + dx, mapWidth - tilesDisplayWidth), 1)
	mapY = math.max(math.min(mapY + dy, mapHeight - tilesDisplayHeight), 1)

	if math.floor(mapX) ~= math.floor(oldMapX) or math.floor(mapY) ~= math.floor(oldMapY) then
		updateTilesetBatch()
		updateUnitTilesetBatch()
	end
end

function selectedTile(x, y)
	selectedX = x
	selectedY = y
	local r, g, b, a = love.graphics.getColor()
	love.graphics.setColor(0, 0, 0, 255)
	love.graphics.rectangle('line', (x - mapX)*tileSize, (y - mapY)*tileSize, tileSize*zoomX, tileSize*zoomY)
	love.graphics.setColor(r, g, b, a)
end

function secondaryTile(x, y)
	secX = x
	secY = y
	local r, g, b, a = love.graphics.getColor()
	love.graphics.setColor(127, 127, 127, 255)
	love.graphics.rectangle('line', x*tileSize, y*tileSize, tileSize*zoomX, tileSize*zoomY)
	love.graphics.setColor(r, g, b, a)
end

function love.mousepressed(x, y, button, istouch)
	if button == 1 then
		-- left click
		local xtile = math.floor(x / tileSize) + math.floor(mapX)
		local ytile = math.floor(y / tileSize) + math.floor(mapY)
		print('Left clicked: ', xtile, ytile)
		selectedTile(xtile, ytile)
		selectedUnit = getSelectedUnit(xtile, ytile)
		print(selectedUnit)
	end

	if button == 2 then
		-- right click
		local xtile = math.floor(x / tileSize) + math.floor(mapX)
		local ytile = math.floor(y / tileSize) + math.floor(mapY)
		local rightClickedUnit = getSelectedUnit(xtile, ytile)
		print('Right clicked: ', xtile, ytile)
		if selectedUnit ~= nil then
			local success = playerTurn == selectedUnit.team
			if rightClickedUnit ~= nil then
				if rightClickedUnit.team == selectedUnit.team then
					-- Friendly unit, can't go there
					success = false
				else
					-- Enemy unit, attack!
					if selectedUnit.AP > 0.0 then
						attack(selectedUnit, rightClickedUnit)
						if rightClickedUnit.HP == 0.0 then
							kill(rightClickedUnit)
							updateUnitTilesetBatch()
						else
							selectedUnit.AP = math.max(selectedUnit.AP - 2.0, 0.0)
							success = false
						end
					else
						success = false
					end
				end
			end

			local success = success and selectedUnit:move(xtile, ytile)

			if success then
				updateUnitTilesetBatch()
				selectedX = xtile
				selectedY = ytile

				turnCheck()
			end
		end
	end
end

function attack(selectedUnit, rightClickedUnit)
	local damage = math.max(selectedUnit.attack - rightClickedUnit.defense, 1.0)
	rightClickedUnit.HP = math.max(rightClickedUnit.HP - damage, 0.0)
	print(string.format('Defender %d/%d HP remaining!', rightClickedUnit.HP, rightClickedUnit.HPmax))
	print(string.format('Attacker %d/%d HP remaining!', selectedUnit.HP, selectedUnit.HPmax))
end

function kill(unit)
	for idx, unitValue in pairs(unitsList) do
		if unitValue == unit then
			table.remove(unitsList, idx)
			break
		end
	end
end

function turnCheck()
	local movementRemaining = 0
	for _, unit in pairs(unitsList) do
		if (unit.team) == playerTurn then
			movementRemaining = movementRemaining + unit.AP
		end
	end
	if movementRemaining == 0 then
		print(string.format('Player %d out of moves!', playerTurn))
		if playerTurn + 1 < numPlayers then
			playerTurn = playerTurn + 1
		else
			playerTurn = 0
			turn = turn + 1
			for _, unit in pairs(unitsList) do
				unit.AP = unit.APmax
			end
		end
		print(string.format('Starting turn %d for player %d!', turn, playerTurn))
	end
end

function getSelectedUnit(xtile, ytile)
	
	for _, unit in pairs(unitsList) do
		if unit.x == xtile and unit.y == ytile then
			return unit
		end
	end

	return nil
end

function love.mousemoved(x, y, dx, dy, istouch)
	local xtile = math.floor(x / tileSize) - (mapX%1)
	local ytile = math.floor(y / tileSize) - (mapY%1)
	secondaryTile(xtile, ytile)
end

function love.keypressed(key)
	if key == "escape" then
		love.event.quit()
	end
end

function love.update(dt)
	if love.keyboard.isDown("lshift") then
		rate = 1.0
	else
		rate = 0.5
	end
	if love.keyboard.isDown("w") then
		moveMap(0, -rate * tileSize * dt)
	end
	if love.keyboard.isDown("s") then
		moveMap(0, rate * tileSize * dt)
	end
	if love.keyboard.isDown("a") then
		moveMap(-rate * tileSize * dt, 0)
	end
	if love.keyboard.isDown("d") then
		moveMap(rate * tileSize * dt, 0)
	end
end

function love.draw()
	--love.graphics.setShader(shader)
	love.graphics.draw(tilesetBatch,     math.floor(-zoomX*(mapX%1)*tileSize), math.floor(-zoomY*(mapY%1)*tileSize), 0, zoomX, zoomY)
	love.graphics.draw(unitTilesetBatch, math.floor(-zoomX*(mapX%1)*tileSize), math.floor(-zoomY*(mapY%1)*tileSize), 0, zoomX, zoomY)
	selectedTile(selectedX, selectedY)
	secondaryTile(secX, secY)
	love.graphics.print("FPS: "..love.timer.getFPS(), 10, 20)
	--love.graphics.setShader()
end

function love.quit()
	print("Goodbye")
end
