Lua-wargame
===========

This is a quick attempt at making a simplistic wargame using lua scripting on top of the [love](https://love2d.org/) game engine.

# What's Working:

- Random heightmap generation using simplex noise
- Simple tileset for map and units
- Unit movement
- Player turns
- Attacking/killing enemy units
- Debug output to console

# What's Not:

- Win conditions
- AI player
- GUI
- Effects of height on battle
- Game menus
- Selection of units for battle based on pts to spend
- Random AI unit selection based on pts to spend